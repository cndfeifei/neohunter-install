sudo apt install tcsh # is required by netMHCpan-4 /usr/bin/tcsh
# sshpass is required if we want to run netMHC command on a remote server

condaforge="-c https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/" # -c conda-forge
bioconda="-c https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/bioconda/" # -c bioconda
pytorch="-c https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/" # -c pytorch
conda=mamba
neohunter=nhh # neohunter

conda install -y mamba -n base -c $condaforge
conda create -y -n $neohunter python=3.10

# common bin, common lib, machine-learning lib, bioinformatics bin, bioinformatics lib
$conda install -y -n $neohunter python=3.10 \
    gcc openjdk perl sshpass tcsh \
    perl-carp-assert psutil pyyaml requests-cache \
    pandas pytorch pytorch-lightning=0.8 scikit-learn xgboost \
    bcftools blast bwa ensembl-vep gatk kallisto optitype samtools snakemake star star-fusion \
    biopython pybiomart pyfasta pysam

