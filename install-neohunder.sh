#conda install -y mamba -n base -c conda-forge

#conda create -y -n neohunter || true
#conda activate neohunter || true

#mamba install -y -c bioconda optitype
#mamba install -y -c bioconda ensembl-vep

bwa index $(dirname $(which OptiTypePipeline.py))/data/hla_reference_rna.fasta

wget -c https://github.com/broadinstitute/gatk/releases/download/4.2.6.1/gatk-4.2.6.1.zip
unzip gatk-4.2.6.1.zip
#wget -c https://github.com/broadinstitute/picard/archive/refs/tags/2.3.0.zip
#unzip 2.3.0.zip
wget -c https://github.com/broadinstitute/picard/releases/download/2.3.0/picard-tools-2.3.0.zip
unzip picard-tools-2.3.0.zip
wget -c https://github.com/milaboratory/mixcr/releases/download/v4.0.0/mixcr-4.0.0.zip
unzip mixcr-4.0.0.zip

## You may have to manually go to the following two web-pages to request for new download links:
	#wget -c https://services.healthtech.dtu.dk/cgi-bin/sw_request?software=netMHCpan&version=4.1&packageversion=4.1b&platform=Linux
	#wget -c https://services.healthtech.dtu.dk/cgi-bin/sw_request?software=netMHCstabpan&version=1.0&packageversion=1.0a&platform=Linux
#wget -c https://services.healthtech.dtu.dk/download/c631f971-f3b3-4504-b4bd-494c313c604b/netMHCpan-4.1b.Linux.tar.gz
#wget -c https://services.healthtech.dtu.dk/download/d5d85deb-8037-4103-b65e-277488900914/netMHCstabpan-1.0a.Linux.tar.gz
#wget -c https://services.healthtech.dtu.dk/cgi-bin/sw_request?software=netMHCpan&version=4.1&packageversion=4.1b&platform=Linux
#tar -xvf netMHCpan-4.1b.Linux.tar.gz
#tar -xvf netMHCstabpan-1.0a.Linux.tar.gz

pushd NetMHCpan-4.1
wget -c https://services.healthtech.dtu.dk/services/NetMHCpan-4.1/data.tar.gz
tar -xvf data.tar.gz
# set NNHOME
popd

pushd netMHCstabpan-1.0/
#wget -c http://www.cbs.dtu.dk/services/NetMHCstabpan-1.0/data.tar.gz
wget -c https://services.healthtech.dtu.dk/services/NetMHCstabpan-1.0/data.tar.gz
tar -xvf data.tar.gz
# set NNHOME
# set NetMHCpan-4.1 PATH
# export TMPDIR=/var/tmp before each run
popd

wget -c https://snpeff.blob.core.windows.net/versions/snpEff_latest_core.zip
unzip snpEff_latest_core.zip

wget -c https://github.com/STAR-Fusion/STAR-Fusion/releases/download/STAR-Fusion-v1.11.0/STAR-Fusion-v1.11.0.FULL.tar.gz
tar -xvf STAR-Fusion-v1.11.0.FULL.tar.gz

wget -c https://github.com/alexdobin/STAR/archive/2.7.8a.tar.gz
tar -xvf 2.7.8a.tar.gz

git clone https://github.com/XuegongLab/NeoHunter.git && cd NeoHunter
bash -evx data_download.sh
