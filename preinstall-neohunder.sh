sudo apt install tcsh # required by /usr/bin/tcsh netMHCpan-4
sudo apt install sshpass # required if we want to run netMHC command on a remote server

condaforge="-c https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/" # -c conda-forge
bioconda="-c https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/bioconda/" # -c bioconda
pytorch="-c https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/" # -c pytorch
conda=mamba

conda install -y mamba -n base -c $condaforge

conda create -y -n neohunter python=3 #3.7.6 #conda activate neohunter || true

#conda run -n neohunter cpan install DB_File
#conda run -n neohunter cpan install URI::Escape
#conda run -n neohunter cpan install Set::IntervalTree
#conda run -n neohunter cpan install Carp::Assert
#conda run -n neohunter cpan install JSON::XS
#conda run -n neohunter cpan install PerlIO::gzip
#conda create -n neohunter-env2

neohunter=neohunter

## OPTITYPE
$conda install -y -n $neohunter $bioconda optitype
#bwa index $(conda env list  | awk -v neohunter=$neohunter '$1 == neohunter' | awk '{print $NF}')/bin/data/hla_reference_rna.fasta

## VEP
$conda install -y -n $neohunter $bioconda ensembl-vep

## ASNEO
#conda create -y -n asneo python=3.7.6
$conda install -y -n $neohunter $bioconda pybiomart #pysam
$conda install -y -n $neohunter $condaforge pandas scikit-learn xgboost # imported by not neeeded by ASNEO

## ERGO
$conda install -y -n $neohunter $condaforge pytorch-lightning=0.8 #1.3
$conda install -y -n $neohunter $pytorch pytorch

## MAIN

neohunter=neohunter

$conda install -y -n $neohunter $bioconda pyfasta

$conda install -y -n $neohunter $bioconda bwa
$conda install -y -n $neohunter $bioconda samtools
$conda install -y -n $neohunter $bioconda bcftools
$conda install -y -n $neohunter $bioconda kallisto
$conda install -y -n $neohunter $bioconda ensembl-vep

$conda install -y -n $neohunter $condaforge biopython
$conda install -y -n $neohunter $condaforge pyyaml
$conda install -y -n $neohunter $condaforge psutil
$conda install -y -n $neohunter $condaforge gcc
$conda install -y -n $neohunter $condaforge openjdk

$conda install -y -n $neohunter $condaforge perl-carp-assert
$conda install -y -n $neohunter $bioconda pysam
$conda install -y -n $neohunter $condaforge request-cache
#$conda install -y urllib3=1.23

## ASNEO pip
conda run -n $neohunter python -m pip install sj2psi # sj2psi

#mamba install -y -c conda-forge tcsh
# required by /usr/bin/tcsh netMHCpan-4

#python -m pip install pyyaml
#python -m pip install psutil
#if false; then
#python -m pip install tqdm
#python -m pip install sj2psi
#python -m pip install torch torchvision pytorch_lightning
#  tar -xvf netMHCpan-4.1b.Linux.tar.gz && tar -xvf netMHCstabpan-1.0a.Linux.tar.gz
#fi

# conda env export > neohunter.freeze.yml &&  conda list -e > neohunter.requirements.txt

